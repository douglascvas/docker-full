FROM docker:stable

RUN for i in 1 2 3 4 5; do \
    apk add --update --no-cache gcc libressl-dev libffi-dev musl-dev python-dev py-pip make \
    && break || sleep 2; done \
    && pip install docker-compose

CMD ["sh"]
